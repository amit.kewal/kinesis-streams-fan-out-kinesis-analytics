CREATE OR REPLACE STREAM "POSITIVE_TRANSACTIONS" (
    "batchId" VARCHAR(200),
    "data" VARCHAR(2000),
    "short_code" VARCHAR(200),
    "display_name" VARCHAR(200)
);


CREATE OR REPLACE PUMP "STREAM_PUMP1" 
    AS INSERT INTO "POSITIVE_TRANSACTIONS"
        SELECT STREAM  
        "batchId", "data" ,"short_code", "display_name"
        FROM "SOURCE_SQL_STREAM_001"
        WHERE "short_code" = 'fanout';

